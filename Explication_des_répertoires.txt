Dans la page de gitlab,il existe différents répertoires dans lesquels on a l'implémentation de chaque page dans le site.

/*----------------------------Répéroitre Présentation des outils--------------------------------*/
---On a dans ce répértoire la présentation des outils qu'on a utilisé(php,html,css,xampp,gitlab).
---1 à 2 pdf pour chaque membre du groupe.
/*---------------------------------------------------------------------------------------------*/

/*--------------------------Répértoire absences_etu--------------------------------------------*/
---Contient un fichier php d'implémentation des absences d'élèves et un fichier de style.
---Permet d'afficher les dates d'absence de l'étudiant connécté.
/*---------------------------------------------------------------------------------------------*/

/*---------------------------------Répértoire absences_prof--------------------------------------------------------------*/
---Contient un fichier php d'implémentation des absences d'élèves et un fichier de style.
---Permet de faire rentrer dans la base de données les dates d'absence des étudiants dans la matière du prof connécté.
/*--------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------Répértoire acceuil------------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page d'acceuil.
---La page d'acceuil permet de se diriger vers la page d'étudiant ou de professeur.
/*-------------------------------------------------------------------------------------------------------------*/

/*-----------------------------Répértoire dashboard----------------------------------------------------------*/
---Contient les fichiers d'implémentatioon du dashboard qui affiche les statistiques de la base de données.
---Chaque professeur a un dashboard suivant sa matière.
/*----------------------------------------------------------------------------------------------------------*/

/*--------------------------Répértoire etudiant-----------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page d'acceuil des étudiants.
/*-------------------------------------------------------------------------------------------------------*/

/*------------------------Répértoire login_etu----------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page du login des étudiants.
/*------------------------------------------------------------------------------------------------------*/

/*-----------------------Répértoire login_prof----------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page du login des professeurs.
/*-------------------------------------------------------------------------------------------------------*/

/*----------------------Répértoire notes_etu------------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page des notes des étudiants.
---La page affiche les notes des étudiants pour chaque matière.
/*-----------------------------------------------------------------------------------------------------*/

/*---------------------Répértoire notes_prof-----------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page des notes des professers.
---La page permet les notes des étudiants pour chaque matière.
/*-----------------------------------------------------------------------------------------------------*/

/*----------------------Répértoire professeur----------------------------------------------------------*/
---Contient les fichiers d'implémentation de la page d'acceuil des professeurs.
/*-----------------------------------------------------------------------------------------------------*/

/*---------------------Répertoire base de données------------------------------------------------------*/
---Contient les fichiers de création et de remplissage de la base de données.
/*-----------------------------------------------------------------------------------------------------*/

/*--------------------Répertoire img------------------------------------------------------------------*/
---Contient les images qu'on a utilisé dans l'implémentation du site web.
/*-----------------------------------------------------------------------------------------------------*/







